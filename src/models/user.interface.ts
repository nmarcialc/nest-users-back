export interface User {
    id: number;
    name: string;
    email: string;
    password: string;
    create_dt: Date;
    update_dt: Date;
}
