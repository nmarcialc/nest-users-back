import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserService } from './user-services/user-services.service';
import { UserEntity } from './entity/user-entity';

import { UserController } from './controller/user.controller';

@Module({

  imports: [
    TypeOrmModule.forFeature([UserEntity])
  ],
  controllers: [UserController],
  providers: [UserService]

})
export class SecurityModule { }
