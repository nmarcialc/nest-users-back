/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserEntity } from '../entity/user-entity';


@Injectable()
export class UserService {

    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>) {

    }
    async saveProduct(user: any) {
        await this.userRepository.insert(user);
        return user;
    }

    async updateProduct(id: number, user: any) {
        await this.userRepository.update(id, user);
    }

    async findAll() {
        return await this.userRepository.find();
    }

    async findOnProduct(id: number) {
        return await this.userRepository.findOne(id);
    }

    async deleteProduct(id: number) {
        return await this.userRepository.delete(id);
    }
}
