/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Controller, Get, Post, Put, Delete, Param, Body } from '@nestjs/common';
import { UserService } from '../user-services/user-services.service';
import { User } from 'src/models/user.interface';

@Controller('users')
export class UserController {

    constructor(private userService: UserService) {
    }

    @Post()
    addProdut(@Body() producModel: User): any {

        return this.userService.saveProduct(producModel);
    }

    @Get()
    getProduct(): any {

        return this.userService.findAll();
    }

    @Get(':id')
    getOneProduct(@Param() params): any {
        return this.userService.findOnProduct(params.id);
    }

    @Put(':id')
    updateProduct(@Body() producModel: User, @Param() params): any {
        return this.userService.updateProduct(params.id, producModel);
    }

    @Delete(':id')
    deleteProducto(@Param() params): any {
        return this.userService.deleteProduct(params.id);
    }
}
