import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { SecurityModule } from '../security/security.module';
import { UserEntity } from '../security/entity/user-entity';

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'mysql',// type database  
            host: 'localhost',//server  database
            port: 3306, // port the database
            username: 'root', //user database
            password: 'P1nnacle!',
            database: 'users-dev',
            entities: [
                UserEntity
            ],
            synchronize: true
        }),
        SecurityModule,

    ]

})
export class DatabaseModule {
    constructor(private readonly connection: Connection) {
    }
}
